@ECHO OFF

REM Unregistering files

regsvr32 /s /u %systemroot%\SYSWOW64\BytescoutSWFToVideo.dll
regsvr32 /s /u %systemroot%\SYSWOW64\BytescoutSWFToVideoFilter.dll

REM now deleting files

del %systemroot%\SYSWOW64\BytescoutSWFToVideo.dll
del %systemroot%\SYSWOW64\BytescoutSWFToVideoFilter.dll

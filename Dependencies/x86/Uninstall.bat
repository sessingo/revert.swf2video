@ECHO OFF

REM Unregistering files

regsvr32 /s /u %windir%\system32\BytescoutSWFToVideo.dll
regsvr32 /s /u %windir%\system32\BytescoutSWFToVideoFilter.dll

REM now deleting files

del %windir%\system32\BytescoutSWFToVideo.dll
del %windir%\system32\BytescoutSWFToVideoFilter.dll

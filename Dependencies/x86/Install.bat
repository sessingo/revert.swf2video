REM change default folder (required for Vista and higher)
CD %~dp0

REM copying files to the system folder

COPY BytescoutSWFToVideoFilter.dll %windir%\system32
COPY BytescoutSWFToVideo.dll %windir%\system32

REM IF NOT EXIST %windir%\system32\gdiplus.dll COPY gdiplus.dll %windir%\system32

REM registering ActiveX/COM server
regsvr32 /s %windir%\system32\BytescoutSWFToVideoFilter.dll
regsvr32 /s %windir%\system32\BytescoutSWFToVideo.dll
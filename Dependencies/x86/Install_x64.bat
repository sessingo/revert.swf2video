REM change default folder (required for Vista and higher)
CD %~dp0

REM This BAT installs x86 versions of DLL into SysWOW64 mode in Windows x64 
REM copying files to the SysWOW64 folder

COPY BytescoutSWFToVideoFilter.dll %systemroot%\SYSWOW64\
COPY BytescoutSWFToVideo.dll %systemroot%\SYSWOW64\

REM IF NOT EXIST %systemroot%\SYSWOW64\gdiplus.dll COPY gdiplus.dll %systemroot%\SYSWOW64\

REM registering ActiveX/COM server
regsvr32 /s %systemroot%\SYSWOW64\BytescoutSWFToVideoFilter.dll
regsvr32 /s %systemroot%\SYSWOW64\BytescoutSWFToVideo.dll
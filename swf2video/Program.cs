﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using BytescoutSWFToVideo;

namespace swf2video
{
	class Program
	{
		public static string InputFile { get; set; }
		public static string OutputFile { get; set; }
		public static int TimeoutMs { get; set; }
		public static int Width { get; set; }
		public static int Height { get; set; }
		static void Main(string[] args)
		{
			int timeoutMs = 15000, width = 800, height = 600; // default settings
			/* Parse arguments */
			for (var i = 0; i < args.Length; i++)
			{
				if (args[i] == "-i" && args[i + 1] != null)
				{
					InputFile = args[i + 1];
				}
				else if (args[i] == "-o" && args[i + 1] != null)
				{
					OutputFile = args[i + 1];
				}
				else if (args[i] == "-t" && args[i + 1] != null)
				{
					int.TryParse(args[i + 1], out timeoutMs);
				}
				else if (args[i] == "-w" && args[i + 1] != null)
				{
					int.TryParse(args[i + 1], out width);
				}
				else if (args[i] == "-h" && args[i + 1] != null)
				{
					int.TryParse(args[i + 1], out height);
				}
			}

			TimeoutMs = timeoutMs;
			Height = height;
			Width = width;

			if (string.IsNullOrEmpty(InputFile))
			{
				Console.WriteLine("Missing required argument input-file (-i)");
				return;
			}

			if (string.IsNullOrEmpty(OutputFile))
			{
				Console.WriteLine("Missing required argument output-file (-o)");
				return;
			}

			// Create an instance of SWFToVideo ActiveX object
			var converter = new SWFToVideo
			{
			    SWFConversionMode = SWFConversionModeType.SWFWithLiveData,
			    InputSWFFileName = @InputFile,
			    OutputVideoFileName = @OutputFile,
				ConversionTimeOut = TimeoutMs,
			    ForcePlayOnceOnLoad = true,
			    FPS = 29.97f,
			    OutputWidth = width,
			    OutputHeight = height
			};
			converter.SWFConversionMode = SWFConversionModeType.SWFWithLiveData;

			// Run conversion 
			converter.SoundRecordingEnabled = false;
			converter.RunAndWait();

			/*while(converter.IsRunning)
			{
				converter.ForcePlay();
			}*/
		}
	}
}
